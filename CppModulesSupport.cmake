#MIT License
#
#Copyright (c) 2022 slavic-demons / korgorusze [Hubert (trebuh.g@gmail.com)]
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

include(${CMAKE_CURRENT_LIST_DIR}/HelperMethods.cmake)

if (NOT CMAKE_CXX_COMPILER_ID  STREQUAL "GNU")
    message(FATAL_ERROR "Only GNU compiler (GCC) is supported")
endif()
if (NOT CMAKE_GENERATOR  STREQUAL "Unix Makefiles")
    message(FATAL_ERROR "Only Unix Makefiles generator is supported")
endif()
if (NOT UNIX)
    message(FATAL_ERROR "Only UNIX filesystem is supported")
endif()


set(DIR_OF_CPP_MODULES_SUPPORT_CMAKE ${CMAKE_CURRENT_LIST_DIR})

# Creates new module library with TARGET_NAME and source specified as ARGN arguments. Target TARGET_NAME can be
# customized with standard cmake approach (target_link_libraries, target_compile options and so on).
# Usage:
# add_module_library(my-module-target module-source1.ixx src/module-source2.cppm src/module-source3.cpp)
function(add_module_library TARGET_NAME)
    set(prefix ADD_MODULE_LIBRARY)
    set(options "")
    set(oneValueArgs "")
    set(multiValueArgs "")
    cmake_parse_arguments(PARSE_ARGV 1 "${prefix}" "${options}" "${oneValueArgs}"
            "${multiValueArgs}")

    if (NOT ADD_MODULE_LIBRARY_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "No module source files specified.")
    endif()

    set(SOURCES_LIST ${ADD_MODULE_LIBRARY_UNPARSED_ARGUMENTS})
    LIST(TRANSFORM SOURCES_LIST PREPEND "${CMAKE_CURRENT_SOURCE_DIR}/")

    # Find module name
    execute_process(
            COMMAND sed -nr "s/^.*module\\s*([_a-zA-Z][_a-zA-Z0-9\\.]*)\\s*;.*$/\\1/p" ${SOURCES_LIST}
            COMMAND head -n 1
            OUTPUT_VARIABLE CAPTURED_MODULE_NAME)
    string(STRIP "${CAPTURED_MODULE_NAME}" CAPTURED_MODULE_NAME)

    # Check if is PMIU (has export module <module name>)
    execute_process(
            COMMAND sed -nr "s/^.*export\\s*module\\s*([_a-zA-Z][_a-zA-Z0-9\\.]*)\\s*;.*$/\\1/p" ${SOURCES_LIST}
            COMMAND head -n 1
            OUTPUT_VARIABLE CAPTURED_MODULE_EXPORT_NAME)
    string(STRIP "${CAPTURED_MODULE_EXPORT_NAME}" CAPTURED_MODULE_EXPORT_NAME)
    if (CAPTURED_MODULE_EXPORT_NAME STREQUAL CAPTURED_MODULE_NAME)
        set(IS_PMIU TRUE)
    else()
        set(IS_PMIU FALSE)
    endif()

    if (NOT CAPTURED_MODULE_NAME AND NOT CAPTURED_MODULE_EXPORT_NAME)
        message(FATAL_ERROR "Module name not found in files: ${SOURCES_LIST}." )
    endif()

    add_library(${TARGET_NAME} STATIC ${ARGN})
    target_compile_options(${TARGET_NAME} PRIVATE -c -x c++)
    set_target_properties(${TARGET_NAME}
            PROPERTIES
            PREFIX ""
            SUFFIX ".pcm"
            MODULE_NAME ${CAPTURED_MODULE_NAME}
            MODULE_IS_PMIU ${IS_PMIU}
            )
    if (IS_PMIU)
        set_target_properties(${TARGET_NAME}
                PROPERTIES
                MODULE_GCM_CACHE_FILE_PATH ${CMAKE_CURRENT_BINARY_DIR}/gcm.cache/${CAPTURED_MODULE_NAME}.gcm
                )
        add_custom_command(TARGET ${TARGET_NAME}
                POST_BUILD
                COMMAND mkdir -p ${CMAKE_BINARY_DIR}/__module_cache/gcm.cache
                COMMAND rm -rf ${CMAKE_BINARY_DIR}/__module_cache/gcm.cache/${CAPTURED_MODULE_NAME}.gcm
                COMMAND ${DIR_OF_CPP_MODULES_SUPPORT_CMAKE}/link_all_files_from_to.py ${CMAKE_CURRENT_BINARY_DIR}/gcm.cache/${CAPTURED_MODULE_NAME}.gcm ${CMAKE_BINARY_DIR}/__module_cache/gcm.cache/${CAPTURED_MODULE_NAME}.gcm
                BYPRODUCTS ${CMAKE_BINARY_DIR}/__module_cache/gcm.cache/${CAPTURED_MODULE_NAME}.gcm)
    endif()
    target_use_header_units(${TARGET_NAME})
endfunction()


# Links defined with add_module_library MODULE_LIBRARY to specified TARGET. Link can be either PUBLIC or PRIVATE.
# Usage:
# target_link_module(my-target PUBLIC | PRIVATE my-module)
function(target_link_module TARGET)
    replace_colons_with_underscores(${TARGET} SANITIZED_TARGET)


    set(prefix TARGET_LINK_MODULE)
    set(options PUBLIC PRIVATE)
    set(oneValueArgs "")
    set(multiValueArgs "")
    cmake_parse_arguments(PARSE_ARGV 1 "${prefix}" "${options}" "${oneValueArgs}"
            "${multiValueArgs}")

    list(LENGTH TARGET_LINK_MODULE_UNPARSED_ARGUMENTS unparsed_length)
    if (NOT unparsed_length EQUAL 1)
        message(FATAL_ERROR "target_link_module should have exactly one module passed as dependency!")
    endif()
    list(GET TARGET_LINK_MODULE_UNPARSED_ARGUMENTS 0 MODULE_LIBRARY)
    replace_colons_with_underscores(${MODULE_LIBRARY} SANITIZED_MODULE_LIBRARY)

    if (TARGET_LINK_MODULE_PUBLIC AND TARGET_LINK_MODULE_PRIVATE)
        message(FATAL_ERROR "target_link_module cannot link in PUBLIC and PRIVATE way at the same time!")
    endif()
    if (NOT TARGET_LINK_MODULE_PUBLIC AND NOT TARGET_LINK_MODULE_PRIVATE)
        message(FATAL_ERROR "target_link_module, specify PUBLIC or PRIVATE link type!")
    endif()

    if (TARGET_LINK_MODULE_PUBLIC)
        set(dependency_link_type PUBLIC)
    else()
        set(dependency_link_type PRIVATE)
    endif()

    add_library(_module_wrapper_${SANITIZED_TARGET}_${SANITIZED_MODULE_LIBRARY} STATIC ${DIR_OF_CPP_MODULES_SUPPORT_CMAKE}/empty.cpp)

    #TODO Maybe this can be done once per target?
    add_custom_command(TARGET _module_wrapper_${SANITIZED_TARGET}_${SANITIZED_MODULE_LIBRARY}
            POST_BUILD
            COMMAND mkdir -p ${CMAKE_CURRENT_BINARY_DIR}/gcm.cache
            COMMAND ${DIR_OF_CPP_MODULES_SUPPORT_CMAKE}/link_all_files_from_to.py ${CMAKE_BINARY_DIR}/__module_cache/gcm.cache ${CMAKE_CURRENT_BINARY_DIR}/gcm.cache/
            BYPRODUCTS ${CMAKE_CURRENT_BINARY_DIR}/gcm.cache
            )

    target_link_libraries(${TARGET} ${dependency_link_type} _module_wrapper_${SANITIZED_TARGET}_${SANITIZED_MODULE_LIBRARY})

    target_link_libraries(_module_wrapper_${SANITIZED_TARGET}_${SANITIZED_MODULE_LIBRARY} PUBLIC ${MODULE_LIBRARY})
endfunction()


# Allows to use header units added with command "add_header_unit" in TARGET. Called automatically on module libraries!
# Usage:
# target_use_header_units(my-target)
function(target_use_header_units TARGET)
    replace_colons_with_underscores(${TARGET} SANITIZED_TARGET)

    set(prefix TARGET_USE_HEADER_UNITS)
    set(options "")
    set(oneValueArgs "")
    set(multiValueArgs "")
    cmake_parse_arguments(PARSE_ARGV 1 "${prefix}" "${options}" "${oneValueArgs}"
            "${multiValueArgs}")

    add_custom_target(_module_wrapper_header_units_${SANITIZED_TARGET}
            COMMAND mkdir -p ${CMAKE_BINARY_DIR}/__module_cache/gcm.cache
            COMMAND mkdir -p ${CMAKE_CURRENT_BINARY_DIR}/gcm.cache
            COMMAND ${DIR_OF_CPP_MODULES_SUPPORT_CMAKE}/link_all_files_from_to.py ${CMAKE_BINARY_DIR}/__module_cache/gcm.cache ${CMAKE_CURRENT_BINARY_DIR}/gcm.cache/
            )

    add_dependencies(${TARGET} _module_wrapper_header_units_${SANITIZED_TARGET})
endfunction()


# Adds system header unit. HEADER_NAME should be system include name. Allows to precompile system header to be used in
# form "import <vector>;". In the future it should be done automatically by the compiler. Tested to work on GCC 12.2.0.
# To allow usage of header units function target_use_header_units has to be called on consumer target.
# target_use_header_units is called automatically on module targets.
# By default uses CXX_STANDARD=-std=c++20.
# Usage:
# add_system_header_unit(vector)
# add_system_header_unit(vector CXX_STANDARD -std=c++23)
function(add_system_header_unit HEADER_NAME)
    set(prefix ADD_SYSTEM_HEADER_UNIT)
    set(options "")
    set(oneValueArgs CXX_STANDARD)
    set(multiValueArgs "")
    cmake_parse_arguments(PARSE_ARGV 1 "${prefix}" "${options}" "${oneValueArgs}"
            "${multiValueArgs}")

    if(NOT ADD_SYSTEM_HEADER_UNIT_CXX_STANDARD)
        set(ADD_SYSTEM_HEADER_UNIT_CXX_STANDARD -std=c++20)
    endif()

    execute_process(
            COMMAND mkdir -p ${CMAKE_BINARY_DIR}/__module_cache/gcm.cache)

    execute_process(
            COMMAND ${CMAKE_CXX_COMPILER} ${ADD_SYSTEM_HEADER_UNIT_CXX_STANDARD} -fmodules-ts -xc++-system-header ${HEADER_NAME}
            WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/__module_cache/)

endfunction()


# Adds header unit in form of a target. HEADER_PATH should be path to header. Allows to precompile header to be used in
# form "import "header_only.h";". In the future it should be done automatically by the compiler. To allow usage of
# header units function target_use_header_units has to be called on consumer target and TARGET_NAME header unit target
# has to be linked by consumer target. target_use_header_units is called automatically on module targets.
# By default uses CXX_STANDARD=-std=c++20.
# Default INCLUDE_DIRECTORY=${CMAKE_CURRENT_SOURCE_DIR} sets include path of created TARGET_NAME target.
# INCLUDE_PATHS should be used if specified header has some include dependencies.
# Usage:
# add_system_header_unit(header_only_library my_header.h)
# add_system_header_unit(header_only_library inc/my_header.h INCLUDE_DIRECTORY inc)
# add_system_header_unit(header_only_library my_header.h CXX_STANDARD -std=c++23)
function(add_header_unit TARGET_NAME HEADER_PATH)
    set(prefix ADD_HEADER_UNIT)
    set(options "")
    set(oneValueArgs CXX_STANDARD INCLUDE_DIRECTORY)
    set(multiValueArgs INCLUDE_PATHS)
    cmake_parse_arguments(PARSE_ARGV 2 "${prefix}" "${options}" "${oneValueArgs}"
            "${multiValueArgs}")

    if(NOT ADD_HEADER_UNIT_CXX_STANDARD)
        set(ADD_HEADER_UNIT_CXX_STANDARD -std=c++20)
    endif()

    if(NOT ADD_HEADER_UNIT_INCLUDE_DIRECTORY)
        set(ADD_HEADER_UNIT_INCLUDE_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
    else()
        get_filename_component(ADD_HEADER_UNIT_INCLUDE_DIRECTORY ${ADD_HEADER_UNIT_INCLUDE_DIRECTORY} ABSOLUTE)
    endif()

    execute_process(
            COMMAND mkdir -p ${CMAKE_BINARY_DIR}/__module_cache/gcm.cache)

    get_filename_component(HEADER_ABSOLUTE_PATH ${HEADER_PATH} ABSOLUTE)
    execute_process(
            COMMAND ${CMAKE_CXX_COMPILER} ${ADD_HEADER_UNIT_CXX_STANDARD} -fmodules-ts -fmodule-header ${HEADER_ABSOLUTE_PATH}
            WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/__module_cache/)

    add_library(${TARGET_NAME} INTERFACE)
    target_include_directories(${TARGET_NAME} INTERFACE ${ADD_HEADER_UNIT_INCLUDE_DIRECTORY})
endfunction()