FROM gcc:12.2.0-bullseye AS base
    ARG DEVELOPER_UID=1000
    RUN groupadd -g 1000 developer && useradd -m --no-log-init -g developer -u $DEVELOPER_UID developer

    RUN apt-get update \
        && apt-get -y install \
        wget \
        && rm -rf /var/lib/apt/lists/*

    # install cmake
    ARG CMAKE_VERSION_MAJOR_MINOR="3.24"
    ARG CMAKE_VERSION_PATCH="2"
    RUN wget https://cmake.org/files/v${CMAKE_VERSION_MAJOR_MINOR}/cmake-${CMAKE_VERSION_MAJOR_MINOR}.${CMAKE_VERSION_PATCH}.tar.gz \
    && tar -xzf cmake-${CMAKE_VERSION_MAJOR_MINOR}.${CMAKE_VERSION_PATCH}.tar.gz \
    && cd cmake-${CMAKE_VERSION_MAJOR_MINOR}.${CMAKE_VERSION_PATCH}/ \
    && ./bootstrap --parallel=$(nproc) \
    && make -j$(nproc) \
    && make install \
    && cd .. \
    && rm -rf cmake-${CMAKE_VERSION_MAJOR_MINOR}.${CMAKE_VERSION_PATCH}.tar.gz \
    && rm -rf cmake-${CMAKE_VERSION_MAJOR_MINOR}.${CMAKE_VERSION_PATCH}

    RUN apt-get update \
        && apt-get -y install \
        gdb \
        && rm -rf /var/lib/apt/lists/*

    USER developer
    WORKDIR /home/developer