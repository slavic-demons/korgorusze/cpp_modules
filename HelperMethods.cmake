
function(replace_colons_with_underscores TARGET OUTPUT)
    string(REPLACE ":" "_" new_string ${TARGET})
    SET(${OUTPUT} ${new_string} PARENT_SCOPE)
endfunction()