add_module_library(math_module
        math.ixx)
target_compile_features(math_module PUBLIC cxx_std_20)
target_compile_options(math_module PUBLIC -fmodules-ts)

add_subdirectory(moduleImpl)
add_subdirectory(moduleFakeImpl)
