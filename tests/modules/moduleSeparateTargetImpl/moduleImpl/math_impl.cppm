module math;

namespace math {

    Number Number::operator+(Number right)
    {
        return Number{value + right.value};
    }

    Number Number::operator-(Number right)
    {
        return Number{value - right.value};
    }


}