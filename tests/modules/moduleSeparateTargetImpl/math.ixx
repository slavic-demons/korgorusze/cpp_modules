export module math; // module declaration
export namespace math {
    class Number
    {
        double value;
    public:
        explicit Number(double value) : value(value) {};

        Number operator+(Number right);
        Number operator-(Number right);
    };
}