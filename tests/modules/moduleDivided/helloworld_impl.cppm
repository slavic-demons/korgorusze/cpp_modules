module;
#include <iostream>

module helloworld.divided; // module declaration

namespace helloworld_divided {
    void hello()       // export declaration
    {
        std::cout << "Hello world divided!\n";
    }
}