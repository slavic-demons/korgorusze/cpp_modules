add_module_library(helloworld_module_divided
        helloworld.ixx
        helloworld_impl.cppm)
target_compile_features(helloworld_module_divided PUBLIC cxx_std_20)
target_compile_options(helloworld_module_divided PUBLIC -fmodules-ts)