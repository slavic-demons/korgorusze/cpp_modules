module;
#include <iostream>
#include <vector>

export module helloworld; // module declaration

import <iostream>;        // import declaration

export void hello()       // export declaration
{
    std::cout << "Hello world!\n";
}

export template<class T>
class Templated
{
public:
    std::vector<T> values;
};