export module helloworld_with_dep; // module declaration

import helloworld.divided;

// Header module units not yet supported by compilers out of the box
//import <iostream>;        // import declaration
namespace helloworld_with_dep {
    export void hello()       // export declaration
    {
        helloworld_divided::hello();
    }
}