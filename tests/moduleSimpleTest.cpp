import helloworld;
import helloworld.divided;
import helloworld_with_dep;
import math;

import "header_only.h";
import <vector>;


int main() {

    hello();
    helloworld_divided::hello();
    helloworld_with_dep::hello();

    math::Number a{2}, b{5};
    a + b;
    a - b;

    Templated<int> vector_class;

    std::vector<double> vector_of_double;

    return 0;
}