#!/usr/bin/env python3

import argparse
import os
import pathlib

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('dir_from', type=pathlib.Path)
    parser.add_argument('dir_to', type=pathlib.Path)
    args = parser.parse_args()

    if args.dir_from.is_file() and not args.dir_to.is_symlink() and not args.dir_to.is_dir() and not args.dir_to.is_file():
            os.system("ln -s {} {}".format(args.dir_from.absolute(), args.dir_to.absolute()))

    if args.dir_from.is_dir() and args.dir_to.is_dir():
        for file in args.dir_from.iterdir():
            symlink = args.dir_to / pathlib.Path(file.name)
            if not symlink.is_symlink() and not symlink.is_dir() and not symlink.is_file():
                os.system("ln -s {} {}".format(file.absolute(), symlink.absolute()))

if __name__ == "__main__":
    main()
