# cpp_modules
Cmake helper commands which allows to use c++20 modules with cmake and makefile generator in basic setup.

[CppModulesSupport.cmake](CppModulesSupport.cmake) presents five cmake functions:
 - add_module_library - creates simple module library in form of static library
 - target_link_module - privately or publicly links previously created module library, also allows to use header units within target
 - target_use_header_units - allows usage of header units created by add_system_header_unit and add_header_unit within target. Called automatically on module targets created by add_module_library
 - add_system_header_unit - adds header unit created from system header
 - add_header_unit - adds header unit from custom header, works only for simple cases, requires linking target dependency by consumer

### Why
Reason of creation of both methods is lack of support from build tools for c++ modules. As the time of writing only MSVC
fully supports modules in its build tools.

For each named module GCC creates CMI (Compiled Module Interface) file of format *.gcm in local directory gcm.cache. 
This file is later needed by any consumer of a module - module's implementation TUs and each importer of the module. 
Currently, neither CMake nor GCC solves problem of making those files accessible by each consumer of module. In the 
future this problem should be solved by more mature implementation of 
[CMI module mapper ](https://gcc.gnu.org/onlinedocs/gcc/C_002b_002b-Module-Mapper.html) in build system or compiler.

### How it works
This library solves this problem by creating symbolic links from module local gcm.cache to global 
__module_cache/gcm.cache in build directory. Later when target uses module all global cache is linked to consumer target
gcm.cache. 

Library also partially supports header units. Allows to create header unit from system library and from custom headers.

Example usage in [CMakeLists.txt](tests%2FCMakeLists.txt) or 
[CMakeLists.txt](tests%2FtransitiveModuleDepTest%2FCMakeLists.txt).

### Prerequisites
Project works only under gcc and with makefile generator. Suggested is usage of the newest gcc compiler. Beside limits 
if build system not everything is implemented in compiler. Modules implementation has limits described 
[here](https://gcc.gnu.org/wiki/cxx-modules). 

Tested to work with gcc:12.2.0 docker (included Dockerfile and docker-compose.yaml file).

### Running project
To build and start docker
```shell
docker compose up -d
docker exec -it cpp_modules-cpp_modules-1 bash
```

To build project
```shell
cmake -S . -B build
cmake --build build
```

### Reference
 - https://github.com/alexpanter/modules_testing/tree/master/gcc-testing/test_07_gameengine/engine
 - https://github.com/premake/premake-core/issues/1735
 - https://gcc.gnu.org/onlinedocs/gcc/C_002b_002b-Modules.html
 - https://www.steinzone.de/wordpress/modules-in-c20-and-cmake/
 - https://www.youtube.com/watch?v=5X803cXe02Y
 - https://github.com/Kitware/CMake/blob/master/Help/dev/experimental.rst
 - https://vector-of-bool.github.io/2019/10/07/modules-3.html